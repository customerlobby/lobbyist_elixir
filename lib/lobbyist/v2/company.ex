defmodule Lobbyist.V2.Company do
  import Lobbyist.Request, only: [get: 1, get: 2, put: 2, post: 2]

  def list do
    get("/v2/companies.json")
  end

  def list(params) do
    get("/v2/companies.json", params)
  end

  def find(id) do
    get("/v2/companies/#{id}.json").company
  end

  def direct_connect_summary(id) do
    get("/v2/companies/#{id}/direct-connect-summary.json")
  end

  def monthly_recurring_revenue(id) do
    get("/v2/companies/#{id}/monthly-recurring-revenue.json")
  end

  def insights(id) do
    get("/v2/companies/#{id}/insights.json")
  end

  def create(company_params \\ []) do
    params = %{company: company_params}
    post("/v2/companies.json", params)
  end

  def update(id, params \\ %{}) do
    put("/v2/companies/#{id}.json", %{company: params})
  end

  def activate(id) do
    put("/v2/companies/#{id}/activate.json",
    %{company: %{is_active: "true", status: "active"}})
  end

  def terminate(id, termination_params \\ %{}) do
    put("/v2/companies/#{id}/terminate.json",
    %{company: %{account_terminated: "true",
                 is_active: "false",
                 termination_date: DateTime.to_string(DateTime.utc_now)},
                 termination: termination_params})
  end

  def reactivate(id, params \\ []) do
    put("/v2/companies/#{id}/reactivate.json", params)
  end

  def scotty_info(id) do
    get("/v2/companies/#{id}/scotty_info.json")
  end

  def debit(company_id, campaign_iteration_id, amount) do
    post("/v2/companies/#{company_id}/debit.json",
    %{campaign_iteration_id: campaign_iteration_id, amount: amount})
  end

  def filtered_list(params \\ %{}) do
    get('/v2/companies/filtered-list.json', params)
  end

end
