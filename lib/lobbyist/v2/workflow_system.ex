defmodule Lobbyist.V2.WorkflowSystem do
  import Lobbyist.Request, only: [get: 1, get: 2, put: 2, post: 2]

  def list(params \\ []) do
    get("/v2/workflow_systems.json", params)
  end

  def create(params \\ []) do
    post("/v2/workflow_systems.json", %{workflow_system: params})
  end

  def find(workflow_system_id) do
    get("/v2/workflow_systems/#{workflow_system_id}.json").workflow_system
  end

   def update(id, params \\ []) do
    put("/v2/workflow_systems/#{id}.json", %{workflow_system: params})
  end
end
