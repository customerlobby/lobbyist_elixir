defmodule Lobbyist.Request do
  use HTTPoison.Base

  def process_request_headers(headers) do
    [ "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Token token=\"#{Lobbyist.api_key}\""
    ] ++ headers
  end

  def get(path) do
    {:ok, response} = super(path)
    handle_response(response)
  end

  def get(path, params) do
    case super(path, params: params) do
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect reason
      {:ok, response} ->
        handle_response(response)
     end
  end

  def handle_response(response) do
    case response.status_code do
      200 -> response.body
      400 -> raise Lobbyist.Error.BadRequest, message: hd(response.body[:errors])
      401 -> raise Lobbyist.Error.Unauthorized, message: hd(response.body[:errors])
      403 -> raise Lobbyist.Error.Forbidden, message: hd(response.body[:errors])
      404 -> raise Lobbyist.Error.NotFound, message: (response.body[:errors])
      412 -> raise Lobbyist.Error.PreconditionFailed, message: hd(response.body[:errors])
      422 -> raise Lobbyist.Error.UnprocessableEntity, message: hd(response.body[:errors])
    end
  end

  def process_request_body(body) do
    Poison.encode!(body)
  end

  def process_response_body(body) do
    dbody = Poison.decode!(body, keys: :atoms)

    case dbody do
      %{count: count, items: items, page: page, rpp: rpp} -> %{count: count, items: items, page: page, rpp: rpp}
      _ -> dbody
    end
  end

  defp process_url(url) do
    Lobbyist.api_base <> url
  end
end
