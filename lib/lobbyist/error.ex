defmodule Lobbyist.Error do
  defmodule BadRequest do
    defexception message: "Bad Request"
  end

  defmodule Unauthorized do
    defexception message: "Unauthorized"
  end

  defmodule Forbidden do
    defexception message: "Forbidden"
  end

  defmodule NotFound do
    defexception message: "Not Found"
  end

  defmodule PreconditionFailed do
    defexception message: "an example error has occurred"
  end

  defmodule UnprocessableEntity do
    defexception message: "an example error has occurred"
  end

  defmodule InternalServerError do
    defexception message: "There was an error processing that request. If the problem persists contact Customer Lobby Support."
  end

end
