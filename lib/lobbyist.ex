defmodule Lobbyist do

  @api_base "http://localhost:3000"
  @api_key "LOBBYIST_API_KEY"
  @api_secret "LOBBYIST_API_KEY"

  @doc """
  The api_base URL as a string.
  
  """
  def api_base, do: @api_base
  def api_key, do: System.get_env(@api_key)
  def api_secret, do: System.get_env(@api_secret)

end