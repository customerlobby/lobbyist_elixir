# config/test.exs
use Mix.Config

config :lobbyist_elixir, api_key:  System.get_env("LOBBYIST_API_KEY")
config :lobbyist_elixir, api_base: System.get_env("LOBBYIST_API_BASE")