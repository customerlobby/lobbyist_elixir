defmodule ExVCR.CompanyTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  doctest Lobbyist.V2.Company

  setup_all do
    HTTPoison.start
  end

  test "raises an exception if no record found" do
    use_cassette "cpmpany_find_missing" do
      assert_raise Lobbyist.Error.NotFound, "Record Not Found.", fn ->
        Lobbyist.V2.Company.find(2)
      end
    end
  end

  test "returns a found record" do
    use_cassette "company_find_found" do
      company = Lobbyist.V2.Company.find(5565)
      assert(company.company_id == 5565)
      assert(company.company_name == "Test1")
    end
  end

  test "updates the company" do
    use_cassette "company_update" do
      {:ok, response} = Lobbyist.V2.Company.update(5565, %{address2: "Something new"})
      assert(response.body.company.address2 == "Something new")
    end
  end

  test "creates a company" do
    use_cassette "company_create" do
      {:ok, response} = Lobbyist.V2.Company.create(%{company_name: "test", phone: 123456789})
      assert(response.body.company.company_name == "test")
    end
  end

end
