# LobbyistElixir

An Elixir client for the Customerlobby API.
It only supports very limited set of functions yet.

##  Tools used

Mix is a build tool that ships with Elixir that provides tasks for creating,
compiling, testing your application, managing its dependencies and much more.
To compare it to ruby Mix is Bundler, RubyGems, and Rake combined.

ExUnit is a test-unit based framework that ships with Elixir;

## Installation

  1. Add lobbyist_elixir to your list of dependencies in `mix.exs`:

        def deps do
          [{:lobbyist_elixir, "~> 0.0.1"}]
        end

  2. Ensure lobbyist_elixir is started before your application:

        def application do
          [applications: [:lobbyist_elixir]]
        end


To install dependencies run
``` mix deps.get ```

## Usage

Example calls

``` Lobbyist.V2.Company.list() ```

``` Lobbyist.V2.Company.find(5565) ```


## Misc information

Some files have .ex as an extension which is for compiled code, and others have .exs (the s stands for script)
which is for interpreted code. Test files for example are .exs which means you don't have to recompile every time
you make a change to a test. If you're writing scripts or tests, use .exs files. Otherwise, just use .ex files
and compile your code.


### Strings and char lists
Strings are represented as Binaries or Character Lists.
Single quotes are different to double quotes.

"hello" is a string (binary) whereas 'hello' is a char list.
'hello' will return a list with the code points for each char.


## Misc commands

```mix test``` will run all tests.

``` iex ```  runs interactive elixir repl

``` iex -S mix ``` includes the current project into the repl

``` h() ``` iex helper - within the repl this can be used to get info on methods etc for example h(IO.puts)

```IEx.pry``` can be used to debug enter the console at certain points (similar to byebug in ruby world). You have to
 run your tests inside an iex session - ``` iex -S mix test ```